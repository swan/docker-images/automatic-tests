# Running automatic tests outside of gitlab

* copy this folder to /tests/
* docker pull gitlab-registry.cern.ch/swan/docker-images/systemuser:v5.1.1 # required for CC7  tests
* docker pull gitlab-registry.cern.ch/swan/docker-images/systemuser:v3.6.0 # required for SLC6 tests
* chmod +x /tests/jupyterhub-singleuser
* run python3 /tests/test_image.py --image  $CC7_id  --stack $release --system cc7 # where $CC7_id is docker image ID for CC7 and $release is for example LCG96
* run python3 /tests/test_image.py --image  $SLC6_id  --stack $release --system slc6  # where $SLC6_id is docker image ID for SLC6 and $release is for example LCG96


# Running automatic tests manually in swan-ci-runner.cern.ch

* login in swan-ci-runner.cern.ch
* clone this repository in a folder different that /tests (We dont want to break the tests in ci) ex: /root/tests
* chmod +x /root/tests/jupyterhub-singleuser
* run python3 /root/tests/test_image.py --image  $CC7_id  --stack $release --system cc7 # where $CC7_id is docker image ID for CC7 and $release is for example LCG96


# Testing pipeline architecture

Swan CI Runner is configured to synchronise this repository to /tests folder on a host
(https://gitlab.cern.ch/ai/it-puppet-hostgroup-swan/blob/qa/code/manifests/ci_runner.pp section Add Notebooks to test), 
so it is available for gitlab pipelines to use Example of such pipeline is https://gitlab.cern.ch/swan/docker-images/systemuser/blob/master/.gitlab-ci.yml


# Adding more tests

To add more notebooks to test new features please do the next steps:

* Write your notebook, clean, without stdout/stderr output in cells or plots and put it in notebooks folder
* in the file tests_descriptor.json write a new entry like:
```sh
{
        "file": "NewNotebook",
        "kernel": "python/ir/root/octave/etc...",
        "from_release": "LCG_XX"
}
```

if you need to add a new stack/platform please do the next steps

* edit the file releases_descriptor.json adding an entry like
```sh
"LCG_XX" : {
        "platforms": {
            "slc6" : ["x86_64-slc6-gccX-opt"],
            "cc7" : ["x86_64-centos7-gccX-opt", "x86_64-centos7-gccY-opt"]
        }
```

now you are ready to run the tests with the procedure above.