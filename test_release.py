import nbformat
from nbconvert.preprocessors import ExecutePreprocessor
from nbconvert.preprocessors.execute import CellExecutionError
import os, json
import pytest

"""
    Test that runs inside a SWAN user session and test that all relevant notebooks are working
"""

base_path = "notebooks"
tests_descriptor = "tests_descriptor.json"
releases_descriptor = "releases_descriptor.json"

python_version = os.environ.get('PYVERSION')
lcg_version = os.environ.get('ROOT_LCG_VIEW_NAME')


def get_list_notebooks():

    json_stacks = open(os.path.join(os.getcwd(), os.path.dirname(__file__), releases_descriptor)).read()
    stacks = json.loads(json_stacks)
    stacks = list(stacks.keys())

    try:
        current_stack = stacks.index(lcg_version)
    except ValueError:
        current_stack = -1

    json_tests = open(os.path.join(os.getcwd(), os.path.dirname(__file__), tests_descriptor)).read()
    notebooks = json.loads(json_tests)

    notebooks_list = []

    for notebook in notebooks:
        notebook_filename = os.path.join(base_path, notebook['file'] + '.ipynb')
        kernel_name = notebook['kernel']
        if kernel_name == 'python':
            kernel_name += python_version

        skip = False
        if current_stack != -1:
            if 'from_release' in notebook:
                skip = (current_stack > stacks.index(notebook['from_release']))
            if 'until_release' in notebook:
                skip = skip or (current_stack < stacks.index(notebook['until_release']))

        timeout = int(notebook['timeout']) if 'timeout' in notebook else 600

        notebooks_list.append((notebook_filename, kernel_name, skip, timeout))

    return notebooks_list

@pytest.mark.parametrize("notebook_filename, kernel_name, skip, timeout", get_list_notebooks())
def test_notebook(notebook_filename, kernel_name, skip, timeout):

    if skip:
        pytest.skip("Incompatible stack")

    with open( os.path.join(os.getcwd(), os.path.dirname(__file__), notebook_filename)) as f:
        nb = nbformat.read(f, as_version=4)
        ep = ExecutePreprocessor(timeout=timeout, kernel_name=kernel_name)
        ep.preprocess(nb, {})
    assert True