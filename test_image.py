
#!/usr/bin/env python

import docker, argparse, time, json, os

releases_descriptor = "releases_descriptor.json"

class StackTester:

    def __init__(self, image, stack, platform):

        self.options = {
            "ports": [],
            "user": "0",
            "environment": {
                "JUPYTERHUB_SERVICE_PREFIX": "/user/admin/",
                "HOME": "/scratch/admin",
                "USER_ID": 100000,
                "JUPYTERHUB_USER": "admin",
                "NB_UID": 100000,
                "JPY_HUB_API_URL": "http://188.184.50.165:8081/hub/api",
                "JPY_COOKIE_NAME": "cookie",
                "JUPYTERHUB_OAUTH_CALLBACK_URL": "/user/admin/oauth_callback",
                "JUPYTERHUB_API_URL": "http://188.184.50.165:8081/hub/api",
                "JUPYTERHUB_BASE_URL": "/",
                "USER": "admin",
                "JUPYTERHUB_CLIENT_ID": "user-admin",
                "JPY_API_TOKEN": "ff2f6d1b9554430798d15c6c9b22744f",
                "JPY_HUB_PREFIX": "/hub/",
                "EXTRA_LIBS": "/extra_libs",
                "JUPYTERHUB_API_TOKEN": "ff2f6d1b9554430798d15c6c9b22744f",
                "NB_USER": "admin",
                "JPY_USER": "admin",
                "JPY_BASE_URL": "/user/admin/",
                "JUPYTERHUB_HOST": "",
                "ROOT_LCG_VIEW_PATH": "/cvmfs/sft.cern.ch/lcg/views"
            },
            "host_config": {
                "Binds": [
                    "/cvmfs:/cvmfs:ro"
                ]
            },
            "volumes": [
                "/cvmfs",
                "/tests",
                "/extra_libs",
                "/usr/local/bin/jupyterhub-singleuser"
            ],
            "command": [
                "sh",
                "/srv/singleuser/systemuser.sh"
            ],
            "working_dir": "/home/admin"
        }

        self.options['name'] = "test-suite-%s-%s-%s" % (image, stack, platform) #to allow concurrent tests of other images
        self.options['image'] = image
        self.options['environment']['ROOT_LCG_VIEW_NAME'] = stack
        self.options['environment']['ROOT_LCG_VIEW_PLATFORM'] = platform
        
        current_path = os.path.join(os.getcwd(), os.path.dirname(__file__))
        self.options['host_config']['Binds'].append("%s:/tests:ro" % current_path)
        self.options['host_config']['Binds'].append("%s:/extra_libs:ro" % os.path.join(current_path, 'extra_libs'))
        self.options['host_config']['Binds'].append("%s:/usr/local/bin/jupyterhub-singleuser:ro" % os.path.join(current_path, 'jupyterhub-singleuser'))

        self.client = docker.APIClient(base_url='unix://var/run/docker.sock')
        self.container = self.client.create_container(**self.options)
        self.container_id = self.container['Id']
        self.client.start(self.container_id)

    def test(self):
        exec_id = self.client.exec_create(self.container_id, 'bash -l /tests/container_script.sh')
        
        result = self.client.exec_start(exec_id)
        print("======== Test output ========\n%s" % result.decode('UTF-8'))

        exec_info = self.client.exec_inspect(exec_id)
        if exec_info['ExitCode']:
            raise TestFailed(exec_info['ExitCode'])

    def stop(self):
        self.client.stop(self.container_id)
        self.client.remove_container(self.container_id)

def execute_test(image, stack, platform):

    print("Starting a test for image %s with stack %s (%s)" % (image, stack, platform))
    tester = StackTester(image, stack, platform)
    print("Waiting for the container to start")
    time.sleep(15)
    print("Testing...")
    try:
        tester.test()
    except:
	# Try the test a 2nd time... (maybe there was a caching problem)
        try:
           tester.test()
        except:
           pass
    finally:
        print("Removing the container")
        tester.stop()


def test_suite():
    parser=argparse.ArgumentParser()
    parser.add_argument('--image')
    parser.add_argument('--system')
    parser.add_argument('--stack')
    parser.add_argument('--platform')
    args=parser.parse_args()

    image = args.image
    stack = args.stack
    platform = args.platform
    system = args.system

    if not image:
        print("No image provided")
        exit(1)

    if not system:
        print("No system provided")
        exit(1)

    try:
        if stack and platform :
            execute_test(image, stack, platform)
            return

        json_stacks = open(os.path.join(os.getcwd(), os.path.dirname(__file__), releases_descriptor)).read()
        stacks = json.loads(json_stacks)

        if stack == "latest":
            stack = next(iter(stacks.keys()))

        if stack in stacks:
            for platform in stacks[stack]['platforms'][system]:
                execute_test(image, stack, platform)
            return
        elif stack:
            print('Stack doesn\'t exist')
            return

        for stack in stacks.keys():
            for platform in stacks[stack]['platforms'][system]:
                execute_test(image, stack, platform)

    except TestFailed as ex:
        print(ex)
        exit(ex.code)


class TestFailed(Exception):

    def __init__(self, code):
        self.code = code

    def __str__(self):
        return "This test failed with the code %s" % self.code


# test_image --stack dev3/latest --platform  x86_64-slc6-gcc62-opt
# test_image --stack LCG_93
# test_image --stack latest
if __name__== "__main__":
    test_suite()
    print("Ending test suite")
