#mkdir /scratch/admin/pytest
pip install --upgrade atomicwrites==1.3.0 attrs==19.3.0 configparser==4.0.2 contextlib2==0.6.0.post1 funcsigs==1.0.2 importlib-metadata==1.5.0 more-itertools==5.0.0 packaging==20.3 pathlib2==2.3.5 pluggy==0.13.1 py==1.8.1 pyparsing==2.4.6 pytest==4.6.0 scandir==1.10.0 six==1.14.0 wcwidth==0.1.8 zipp==1.2.0  --target /scratch/admin/pytest
cp -r /tests/ /automated-tests
cp /automated-tests/.Rprofile /home/admin/
python /scratch/admin/pytest/pytest.py -v /automated-tests/test_release.py
