from notebook import transutils #needs to be imported before Jupyter File Manager
from notebook.services.contents.largefilemanager import LargeFileManager

# Provide the default filemanger, as it's not needed for the tests.
# This file needs to exist because the configuration of the docker image
# expects it.

class SwanFileManager(LargeFileManager):
    pass
